
package com.dao;

import java.util.List;
import java.util.ArrayList;
import org.hibernate.Transaction;
import org.hibernate.Session;
import com.util.HibernateUtil;
import com.model.pojo.User;
import javax.faces.application.FacesMessage;
import org.primefaces.context.RequestContext;

/**
 * 
 * @author tinix
 */
public class UserDAO {
	private User user;
	private User newuser;
	private List<User> daoAllUsers;
	private List<User> DaoSearchUserList;

	// Session session;
	public List<User> AllUsers() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			session.beginTransaction();
			daoAllUsers = session.createNativeQuery("SELECT * FROM persona", User.class).list();

			int count = daoAllUsers.size();
			System.out.println("Cantidad de Personas: " + count);
			// FacesMessage message1 = new FacesMessage(FacesMessage.SEVERITY_INFO, "List
			// Size", Integer.toString(count));//Debugging Purpose
			// RequestContext.getCurrentInstance().showMessageInDialog(message1);
			session.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
		}
		session.close();
		return daoAllUsers;
	}

	public List<Integer> getIdUser() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		String query = "SELECT id_persona FROM persona p";
		 List <Integer> results = session.createNativeQuery(query).list();
		session.close();
		return results;
	}

	public List<User> SearchByRecordNo(String RecordNo) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		List<User> daoSearchList = new ArrayList<>();
		try {
			session.beginTransaction();
			Query qu = session.createQuery("From User U where U.recordNo =:recordNo"); // User is the entity not the
																						// table
			qu.setParameter("recordNo", RecordNo);
			daoSearchList = qu.list();
			int count = daoSearchList.size();
			session.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
		} finally {
			session.close();
		}
		return daoSearchList;
	}

	public void add(User newuser) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			String Id = Integer.toString(newuser.getIdPersona());
			// begin a transaction
			session.beginTransaction();
			session.merge(newuser);
			session.flush();
			System.out.println("NewUser saved, id: " + newuser.getIdPersona());
			session.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
		}
		session.close();
	}

	public void delete(User user) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			String name = user.getName();
			session.beginTransaction();
			session.delete(user);
			session.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
		}
		session.close();
	}

	public void update(User user) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			session.beginTransaction();
			session.update(user);
			session.flush();
			session.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
		}
		session.close();
	}
}
