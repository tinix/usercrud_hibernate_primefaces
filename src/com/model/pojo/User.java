package com.model.pojo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "persona")
public class User implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private Integer idPersona;
	private String nombre;
	private String apellido;
	private String dni;
	private String domicilio;
	private String profesion;

	public User() {
	}

	public User(Integer idPersona, String nombre, String apellido, String dni, String domicilio, String profesion) {
		this.idPersona = idPersona;
		this.nombre = nombre;
		this.apellido = apellido;
		this.dni = dni;
		this.domicilio = domicilio;
		this.profesion = profesion;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id_persona", unique = true, nullable = false)
	public Integer getIdPersona() {
		return idPersona;
	}

	public void setIdPersona(Integer idPersona) {
		this.idPersona = idPersona;
	}
	@Column(name = "nombre", length = 45) 
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	@Column(name = "apellido", length = 45) 
	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	@Column(name = "dni", length = 445) 
	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}
	@Column(name = "domicilio", length = 150) 
	public String getDomicilio() {
		return domicilio;
	}

	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}
	@Column(name = "profesion", length = 100) 
	public String getProfesion() {
		return profesion;
	}

	public void setProfesion(String profesion) {
		this.profesion = profesion;
	}

	@Override
	public String toString() {
		return "User [idPersona=" + idPersona + ", nombre=" + nombre + ", apellido=" + apellido + ", dni=" + dni
				+ ", domicilio=" + domicilio + ", profesion=" + profesion + "]";
	}
	

}
